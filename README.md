# Function Plotter

## Installation

**install needed packages using pip:**  
* extract project to desired folder  
* open console and navigate inside project folder
```
cd "C:/your/desired/project/folder"
```
* install all required packages using the following command:
```
pip install -r requirements.txt
```

## About
### What does it do?
This is a function plotter written in python using the tkinter package.  
With it you can see a graphical presentation of your desired mathematical function.

This program offers you the possibility to save your plotted functions into a picture.

### Functionality
* Create new drawing boards / open saved drawing board
* Each board holds any number of mathematical functions (polynomials) to plot
* Functions on a board are plotted together, so you may compare them
* Add new functions to a drawing board on-the-fly or remove idle functions
* Hide/Display a function or change color of the function's curve
* Zoom drawing board and adjust drawing precision
* Save drawing board into a picture (export)

