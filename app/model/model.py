import sqlite3


class Model:
    """
    Model class with connection to SQLite database.
    """

    ####################################################################################################################
    # Constructor ######################################################################################################

    def __init__(self):

        # Opens database connection ####################################################################################
        try:
            self.connection = sqlite3.connect('./app/.fp_database')  # creates file, if not exists
            self.cursor = None
        except sqlite3.OperationalError as error:
            print(error)
            pass
        else:
            self.init_database()

    # Initializes a new database and create necessary tables ###########################################################
    def init_database(self):
        sql_board = 'CREATE TABLE board (name VARCHAR(100) NOT NULL, UNIQUE(name))'
        sql_function = '''
            CREATE TABLE function (
                boardID INT NOT NULL,
                function VARCHAR(255),
                color VARCHAR(7) DEFAULT '#000000',
                visible INT NOT NULL DEFAULT 1,
                CONSTRAINT fk_board 
                    FOREIGN KEY (boardID) 
                    REFERENCES board(boardID) 
                    ON DELETE CASCADE
            )
            '''
        try:
            cursor = self.connection.cursor()
            cursor.execute(sql_board)
            cursor.execute(sql_function)
            self.connection.commit()
        except Exception as error:
            if 'table board already exists' in str(error):
                pass  # ignore this error
            else:
                print('Error while creating database:', str(error))
                raise Exception(error)

    # Executes a SQL query #############################################################################################
    # Param sql_str: The SQL query as string
    # Param commit: Set auto commit as boolean
    # Returns result of SQL transaction
    def query(self, sql_str, commit=False):
        try:
            self.cursor = self.connection.cursor()
            self.cursor.execute(sql_str)

            if sql_str.startswith('INSERT'):
                return self.cursor.lastrowid
            elif sql_str.startswith('SELECT'):
                return self.cursor.fetchall()
            elif sql_str.startswith('UPDATE') or sql_str.startswith('DELETE'):
                return True
            else:
                raise Exception('Cannot identify SQL string')
        except Exception as error:
            raise Exception(error)
        finally:
            if commit:
                self.commit()

    # Commits changes to database ######################################################################################
    def commit(self):
        self.connection.commit()

    # Discards uncommitted changes #####################################################################################
    # Passes exceptions if no cursor/transaction exists
    def discard(self):
        try:
            self.cursor.execute('rollback')
        except Exception as error:
            pass

    # Closes database connection #######################################################################################
    def close(self):
        self.connection.close()
