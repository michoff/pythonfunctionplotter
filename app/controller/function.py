class Function:
    """
    Function class represents a mathematical polynomial.
    """

    ####################################################################################################################
    # Constructor ######################################################################################################

    def __init__(self, board_id, id=None, function=None, color='#000000', visible=1):

        # Class variables
        self.board_id = board_id  # ID of dedicated board as integer
        self.id = id  # Database ID of function as integer
        self.function = function  # The mathematical function as string
        self.color = color  # Color of function as hex string

        # Visibility of function as 0 (False, hidden) or 1 (True, visible)
        self.visible = self.__visible_converter(visible)

    # PRIVATE: Converts integer to boolean #############################################################################
    # Param visible_int: Integer represents visibility
    # Returns boolean
    @staticmethod
    def __visible_converter(visible_int):
        if int(visible_int) == 1:
            return True
        else:
            return False
