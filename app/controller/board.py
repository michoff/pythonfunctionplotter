import re
from app.controller.function import Function


class Board:
    """
    Board class represents a canvas board with related functions.
    """

    ####################################################################################################################
    # Constructor ######################################################################################################
    def __init__(self, model, board_id=None, name=None):

        # Class variables
        self.model = model  # Instance of model class
        self.id = board_id  # Database ID of board
        self.name = name  # Name of board
        self.function_list = []  # Array with according functions of board

        # Adds a new board or opens one by ID
        self.__add_new_board() if self.id is None else self.__open_board()

    # PRIVATE: Adds a new board to database ############################################################################
    def __add_new_board(self):
        try:
            if self.name is not None and len(self.name) > 0:
                sql_query = 'INSERT INTO board (name) VALUES (\'{name}\')'
                self.id = self.model.query(sql_query.format(name=self.name.strip()))  # do not commit
            else:
                raise Exception('No board name set')
        except Exception as error:
            if 'UNIQUE constraint failed' in str(error):
                raise Exception('Board name \'{name}\' already exists'.format(name=self.name))
            else:
                raise Exception(error)

    # PRIVATE: Opens an existing board #################################################################################
    def __open_board(self):
        try:
            sql_query = 'SELECT name FROM board WHERE rowid={id}'
            self.name = self.model.query(sql_query.format(id=int(self.id)))[0][0]
        except IndexError:
            raise Exception('No board found for ID {id}'.format(id=int(self.id)))
        except Exception as error:
            raise Exception(error)

    ####################################################################################################################
    # Function methods #################################################################################################

    # Loads all functions of a board and store in functions array ######################################################
    def fn_load(self):
        sql_query = 'SELECT rowid, * FROM function WHERE boardID={id}'
        fresult = self.model.query(sql_query.format(id=int(self.id)))
        if len(fresult) > 0:
            for f in fresult:
                self.function_list.append(Function(self.id, f[0], f[2], f[3], f[4]))

    # Takes an index for the function array and returns the corresponding database id ##################################
    def get_id_by_idx(self, idx):
        try:
            idx -= 1
            function = self.function_list[idx]
            if function:
                return function.id
            else:
                raise IndexError()
        except IndexError:
            return 0
        except Exception as e:
            print(e)
            raise Exception('Cannot fetch function ID by array index')

    # returns the visibility status of the given function ##############################################################
    def get_visibility(self, idx):
        idx -= 1
        function = self.function_list[idx]
        if function:
            return function.visible
        else:
            raise IndexError()

    # Adds a new function to functions array list ######################################################################
    # Param function_str: Mathematical polynomial as string
    def fn_add(self, function_str):
        sql_query = 'INSERT INTO function (boardID, function) VALUES ({id}, \'{fn}\')'
        fn_id = self.model.query(sql_query.format(id=self.id, fn=function_str))  # do not commit
        self.function_list.append(Function(self.id, fn_id, function_str))

    # Changes color of a function by ID ################################################################################
    # Param func_id: ID of function as integer
    # Param color: New color as hex string
    def fn_color(self, func_id, color):
        if re.match(r'^#([0-9a-f]{6}|[0-9a-f]{3})$', color):
            for idx, f in enumerate(self.function_list):
                if f.id == func_id:
                    sql_query = 'UPDATE function SET color=\'{c}\' WHERE rowid={id}'
                    self.model.query(sql_query.format(c=color, id=func_id))
                    f.color = color
                    break
            else:
                raise Exception('Function ID {id} not found'.format(id=func_id))
        else:
            raise Exception('Invalid hex color string syntax')

    # Changes visibility of function by ID #############################################################################
    # Param func_id: ID of function as integer
    def fn_change_visibility(self, func_id):
        for idx, f in enumerate(self.function_list):
            if f.id == func_id:
                f.visible = 0 if f.visible == 1 else 1
                sql_query = 'UPDATE function SET visible=\'{v}\' WHERE rowid={id}'
                self.model.query(sql_query.format(v=f.visible, id=func_id))
                break
        else:
            raise Exception('Function ID {id} not found'.format(id=func_id))

    # Removes function #################################################################################################
    # Param func_id: ID of function as integer
    def fn_remove(self, func_id):
        for idx, f in enumerate(self.function_list):
            if f.id == func_id:
                self.model.query('DELETE FROM function WHERE rowid={id}'.format(id=func_id))
                self.function_list.pop(idx)
                break
        else:
            raise Exception('Function ID {id} not found'.format(id=func_id))
