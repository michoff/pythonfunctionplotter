from app.model.model import Model
from app.controller.board import Board
from app.controller.parser import Parser


class Controller:
    """
    Controller class interacts between view class and model class.
    """

    ####################################################################################################################
    # Constructor ######################################################################################################

    def __init__(self):

        # Class variables
        self.model = Model()  # Instance of model class
        self.board = None  # Instance of board class

    ####################################################################################################################
    # Board methods ####################################################################################################

    # Opens existing board by ID #######################################################################################
    # Param id: ID of board as integer
    def load_board(self, id):
        self.board = Board(self.model, id)
        self.board.fn_load()

    # Creates a new board with name ####################################################################################
    # Param name: Name of board as string
    def new_board(self, name):
        self.board = Board(self.model, None, name)

    # Fetches all stored boards ########################################################################################
    # Returns tuple
    def get_all_boards(self):
        return self.model.query('SELECT rowid, * FROM board')

    # Deletes selected board ###########################################################################################
    # Returns boolean
    def delete_board(self, id):
        sql_query = 'DELETE FROM board WHERE rowid={id}'
        result = self.model.query(sql_query.format(id=id), True)  # commit
        if self.board.id == id:
            self.board = None
        return result

    # Gets all functions of this board #################################################################################
    # Returns tuple
    def get_board_functions(self):
        if self.board is not None:
            return self.board.function_list
        else:
            raise Exception('No board selected')

    ####################################################################################################################
    # Function methods #################################################################################################

    # Adds a new function to the board's functions array ###############################################################
    # Param function_str: Mathematical polynomial as string
    def new_function(self, function_str):
        if self.board is not None:
            parsed_function_str = Parser.evaluate(function_str)
            if parsed_function_str:
                self.board.fn_add(parsed_function_str)
            else:
                raise Exception('Invalid function syntax')
        else:
            raise Exception('No board selected')

    # Changes the color (hex value) of a board's function ##############################################################
    # Param id: ID of function as integer
    # Param color: New color as hex string
    def change_function_color(self, id, color):
        if self.board is not None:
            self.board.fn_color(int(id), color.strip().lower())
        else:
            raise Exception('No board selected')

    # Changes function visibility ######################################################################################
    # Param id: ID of function as integer
    def change_function_visibility(self, id):
        if self.board is not None:
            self.board.fn_change_visibility(int(id))
        else:
            raise Exception('No board selected')

    # Deletes a function ###############################################################################################
    # Param id: ID of function as integer
    def delete_function(self, id):
        if self.board is not None:
            self.board.fn_remove(int(id))
        else:
            raise Exception('No board selected')

    # Gets the visibility status of the given function #################################################################
    def get_function_visibility(self, idx):
        if self.board is not None:
            return self.board.get_visibility(idx)
        else:
            raise Exception('No board selected')

    ####################################################################################################################
    # Logical methods ##################################################################################################

    # Saves the selected board to database persistently ################################################################
    def save_board_to_database(self):
        self.model.commit()

    # Examines if a board is selected ##################################################################################
    # Returns boolean
    def examine_board_selected(self):
        if self.board is None:
            raise Exception('No board selected')
        return True

    # Discards uncommitted changes in database #########################################################################
    def discard_changes(self):
        self.model.discard()

    # Closes connection to database ####################################################################################
    def close_database(self):
        self.model.close()
