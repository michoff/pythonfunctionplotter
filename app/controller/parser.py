import re
import numpy


class Parser:
    """
    Parser class to validate and evaluate a mathematical polynomial.
    """

    # Mathematical functions ###########################################################################################
    mfunc = ['sin', 'cos', 'tan', 'arcsin', 'tanh', 'pi']

    # Validates and parses a function string ###########################################################################
    # Returns parsed function string
    @staticmethod
    def evaluate(fn_str):
        try:
            if fn_str:
                regex = re.compile(r'^[\s0-9x^+\-*/\(\)(' + '|'.join(Parser.mfunc) + ')]+$')
                parsed_fn_str = fn_str.replace('^', '**')
                parsed_fn_str = re.sub(r'(' + '|'.join(Parser.mfunc) + ')', r'numpy.\1', parsed_fn_str)

                # Checks if function returns a result with initial value for x
                # Ignores errors like DivideByZero exception
                with numpy.errstate(divide='ignore'):
                    x = 1  # initial value
                    p = eval(parsed_fn_str)

                if regex.match(fn_str) and p:
                    return parsed_fn_str
                else:
                    raise Exception('Invalid function string')
            else:
                raise Exception('Empty function string')
        except ZeroDivisionError:
            raise Exception('Function contains division by zero')
        except (SyntaxError, Exception) as e:
            print(e)
            raise Exception('Function has an invalid syntax')

    # Calculates xy-values of the given function #######################################################################
    # Sets y value to inf (infinite) if division by zero occurs like 1/x
    # Param parsed_str: The mathematical polynomial as parsed function string
    # Param xrange: Value range on x-axis (default: -50 to 50)
    # Param yrange: Value range on y-axis (default: -50 to 50)
    # Param prec:   Precision of calculating steps
    # Param xsteps: Steps on x-axis (default: 1)
    # Param ysteps: Steps on y-axis (default: 1)
    # Returns array with xy-value tuples
    @staticmethod
    def calc(parsed_str, xrange=50.0, yrange=50.0, prec=0.5, xsteps=1.0, ysteps=1.0):
        try:
            coords = []
            for x in numpy.arange(-float(xrange), float(xrange), prec):
                with numpy.errstate(divide='ignore'):  # ignores division by zero error
                    y = eval(parsed_str) / float(ysteps)
                    if y == numpy.Infinity:
                        coords.append((x / float(xsteps), 'inf'))
                    if y > float(yrange) or y < -float(yrange):
                        continue
                    coords.append((x / float(xsteps), y))
            return coords
        except Exception as error:
            print('Error while calculating coordinates:', error)
            return []
