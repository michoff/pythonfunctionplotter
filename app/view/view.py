# TKinter imports
from tkinter import *
from tkinter import messagebox
from tkinter import simpledialog
from tkinter import colorchooser
import tkinter as tk

# Pillow imports
from PIL import Image
from PIL import ImageDraw

# System imports
import os
import sys

# Thread import
import threading

# Class imports
import app.view.zoomhelper as zoomhelp
import app.resources.res as resources
from app.controller.controller import Controller
from app.controller.parser import Parser as Parse

# Constants
_CANVAS_LENGTH = 600
_HALF_CANVAS_LENGTH = _CANVAS_LENGTH * 0.5
_CENTER = _HALF_CANVAS_LENGTH
_MAIN_WINDOW_WIDTH = 919
_MAIN_WINDOW_HEIGHT = 600

# Some color constants for PIL
_WHITE = (255, 255, 255)
_BLACK = (0, 0, 0)
_BLUE = (0, 0, 255)
_RED = (255, 0, 0)
_GREEN = (0, 128, 0)
_GREY = (217, 217, 217)

# Initial drawing value for xy-axis
_DEFAULT_X_STEPS = 1.0
_DEFAULT_Y_STEPS = 1.0
_DEFAULT_X_RANGE = 50.0
_DEFAULT_Y_RANGE = 999999.0

# Picture types for export
_TYPES = ['.png', '.jpg', '.gif', '.bmp']


class View:
    """
    View class generates and displays the graphical user interface (GUI).
    """

    ####################################################################################################################
    # Constructor ######################################################################################################

    def __init__(self):

        # Initializing controller / model ##############################################################################
        # Quits program if initialization fails ########################################################################
        try:
            self.__controller = Controller()
        except Exception as e:
            self._show_error(e)
            sys.exit(1)

        # GUI variables ################################################################################################
        self.__main_window = tk.Tk()
        self.__main_window.title("Function Plotter")
        self.__main_window.resizable(FALSE, FALSE)
        self.__canvas_frame = Frame(self.__main_window)
        self.__canvas = None

        # Plotting variables ###########################################################################################
        self.__zoom_index = 12
        self.__zoom_factor = zoomhelp.array_func(_CANVAS_LENGTH)
        self.__spacing = self.__zoom_factor[self.__zoom_index]
        self.__show_marker_value = True  # false if zoomed out too much
        self.__marker_font_size = 16  # dynamicly changes with zoom
        self.__draw = None
        self.__image_tmp = None
        self.__precision = 0.5
        self.__xsteps = _DEFAULT_X_STEPS
        self.__ysteps = _DEFAULT_X_STEPS
        self.__xrange = _DEFAULT_X_RANGE
        self.__yrange = _DEFAULT_Y_RANGE
        self.__function_frame = None

        ################################################################################################################
        # Menu section #################################################################################################

        # First menu column ############################################################################################
        __menu = Menu(self.__main_window)
        self.__main_window.config(menu=__menu)
        __board_menu = Menu(__menu)

        __menu.add_cascade(label="File", menu=__board_menu)
        __board_menu.add_command(label="New", command=lambda: self.__method_picker('_new_board'))
        __board_menu.add_command(label="Load", command=lambda: self.__method_picker('_load_board_menu'))
        __board_menu.add_command(label="Save", command=lambda: self.__method_picker('_save_board'))
        __board_menu.add_command(label="Delete", command=lambda: self.__method_picker('_delete_board_window'))
        __board_menu.add_separator()
        __board_menu.add_command(label="Export", command=lambda: self.__method_picker('_export_board'))
        __board_menu.add_separator()
        __board_menu.add_command(label="Exit", command=lambda: self.__method_picker('_on_close_window'))

        # Second menu column ###########################################################################################
        __function_menu = Menu(__menu)
        __menu.add_cascade(label="Function", menu=__function_menu)
        __function_menu.add_command(label="Add", command=lambda: self.__method_picker('_add_function'))

        # Third menu column ############################################################################################
        __help_menu = Menu(__menu)
        __menu.add_cascade(label="Help", menu=__help_menu)
        __help_menu.add_command(label="About...", command=lambda: self.__method_picker('_open_about_window'))

        # Drawing Canvas and creates a drawing file for saving of the canvas ###########################################
        self.__draw_canvas()

        # Adds transformation buttons ##################################################################################
        self.__add_transformation_buttons()  # left column on main window

        # Setup for spawning the main window, only called once #########################################################
        # Gets screen width and height
        self.__screen_width = self.__main_window.winfo_screenwidth()  # width of the screen
        self.__screen_height = self.__main_window.winfo_screenheight()  # height of the screen

        # Calculates x and y coordinates for the Tk root window ########################################################
        self.__spawn_x = (self.__screen_width / 2) - (_MAIN_WINDOW_WIDTH / 2)
        self.__spawn_y = (self.__screen_height / 2) - (_MAIN_WINDOW_HEIGHT / 2)
        self.__spawn_geometry = '%dx%d+%d+%d' % (_MAIN_WINDOW_WIDTH,
                                                 _MAIN_WINDOW_HEIGHT,
                                                 self.__spawn_x,
                                                 self.__spawn_y)

        # Sets screen dimensions and where it is placed ################################################################
        self.__main_window.geometry(self.__spawn_geometry)

        # Starting main loop ###########################################################################################
        self.__main_window.protocol('WM_DELETE_WINDOW', lambda: self.__method_picker('_on_close_window'))
        mainloop()

    # Sends a confirmation dialog if user closes the window ############################################################
    def _on_close_window(self):
        if messagebox.askokcancel("Close", "Close window? Unsaved changes will be lost."):
            self.__controller.close_database()
            self.__main_window.quit()

    ####################################################################################################################
    # Canvas and file drawing section ##################################################################################

    # Changes the value of calculating precision #######################################################################
    # Param value: Precision value as integer
    def _change_precision(self, value):
        self.__precision *= value
        self.__update_canvas()

    # Zooms the canvas in or out #######################################################################################
    # Param zoom_index_change: Value to de-/increment can be of value 1 or -1
    def _zoom_canvas(self, zoom_index_change):
        self.__zoom_index += zoom_index_change
        # Checks if the zoom_index would be out of bounds
        if self.__zoom_index < 0:
            self.__zoom_index += 1
            return
        if self.__zoom_index >= len(self.__zoom_factor) - 1:
            self.__zoom_index -= 1
            return
        self.__spacing = self.__zoom_factor[self.__zoom_index]
        self.__update_canvas()

    # PRIVATE: Updates canvas field ####################################################################################
    def __update_canvas(self):
        self.__canvas.delete(ALL)  # Canvas setup
        self.__draw_coordinate_system()  # Draws empty coordinate system
        self.__draw_canvas_pil()  # Draws empty file .ps
        if self.__function_frame is not None:
            self.__delete_function_frame()

        if self.__controller.board is not None:
            self.__add_function_buttons()  # Draw function buttons
            self.__draw_functions()  # Draws functions
        self.__canvas.update()  # Shows changes on canvas

    # PRIVATE: Draws the canvas ########################################################################################
    # is only called once by initialization
    def __draw_canvas(self):
        self.__canvas_frame = Frame(self.__main_window, bg=self.__get_hex_color(_GREEN))
        self.__canvas_frame.grid(column=1, row=0)

        # Canvas setup
        self.__canvas = Canvas(self.__canvas_frame, height=_CANVAS_LENGTH, width=_CANVAS_LENGTH)
        self.__canvas.configure(background="white")
        self.__canvas.configure(bd=-2)  # removes canvas build in padding

        # Draws empty coordinate system
        self.__draw_coordinate_system()
        self.__canvas.pack()

        # Does everything for PIL too, so that the plotted functions can be saved
        self.__draw_canvas_pil()

    # PRIVATE: Draws canvas for PIL library ############################################################################
    def __draw_canvas_pil(self):
        # PIL creates an empty image and draws objects to draw on memory only, not visible
        self.__image_tmp = Image.new("RGB", (_CANVAS_LENGTH, _CANVAS_LENGTH), _WHITE)
        self.__draw = ImageDraw.Draw(self.__image_tmp)

        # Creates printing instructions
        self.__canvas.postscript(file="my_drawing.ps", colormode='color')

        # Draws coordinate system
        self.__draw_coordinate_system_pil()

    # PRIVATE: Adds markers to the coordinate system ###################################################################
    def __draw_coordinate_system(self):
        marks = self.__spacing * 0.3
        font_size = int((self.__marker_font_size * self.__spacing) * 0.01)

        # Auto hide marker's values if font size is less than 3
        self.__show_marker_value = True if font_size >= 4 else False

        # Draw grid
        for y in range(self.__spacing, _CANVAS_LENGTH, self.__spacing):
            self.__canvas.create_line(y, 0, y, _CANVAS_LENGTH, fill="#d9d9d9")  # vertical line (y-axis)
        for x in range(self.__spacing, _CANVAS_LENGTH, self.__spacing):
            self.__canvas.create_line(0, x, _CANVAS_LENGTH, x, fill="#d9d9d9")  # horizontal line (x-axis)

        # Draw markers
        # y-axis
        counter = 1.0
        for y in range(self.__spacing, _CANVAS_LENGTH, self.__spacing):
            self.__canvas.create_line(_HALF_CANVAS_LENGTH - marks, y, _HALF_CANVAS_LENGTH + marks, y)

            # Draws marker's values on y-axis
            if self.__show_marker_value:
                self.__canvas.create_text(_HALF_CANVAS_LENGTH - marks * 2, _HALF_CANVAS_LENGTH + y,
                                          font=('Arial', font_size), text=float(self.__ysteps) * counter * -1.0)
                self.__canvas.create_text(_HALF_CANVAS_LENGTH - marks * 2, _HALF_CANVAS_LENGTH - y,
                                          font=('Arial', font_size), text=float(self.__ysteps) * counter)
                counter += 1.0

        # x-axis
        counter = 1.0
        for x in range(self.__spacing, _CANVAS_LENGTH, self.__spacing):
            self.__canvas.create_line(x, (_HALF_CANVAS_LENGTH + marks), x, _HALF_CANVAS_LENGTH - marks)

            # Draws marker's values on x-axis
            if self.__show_marker_value:
                self.__canvas.create_text(_HALF_CANVAS_LENGTH + x, _HALF_CANVAS_LENGTH + marks * 2,
                                          font=('Arial', font_size), text=float(self.__xsteps) * counter)
                self.__canvas.create_text(_HALF_CANVAS_LENGTH - x, _HALF_CANVAS_LENGTH + marks * 2,
                                          font=('Arial', font_size), text=float(self.__xsteps) * counter * -1.0)
                counter += 1.0

        # Draws x-y lines
        self.__canvas.create_line(0, _HALF_CANVAS_LENGTH, _CANVAS_LENGTH, _HALF_CANVAS_LENGTH)  # x-axis
        self.__canvas.create_line(_HALF_CANVAS_LENGTH, 0, _HALF_CANVAS_LENGTH, _CANVAS_LENGTH)  # y-axis

    # PRIVATE: Adds markers to the PIL's coordinate system for saving images ###########################################
    def __draw_coordinate_system_pil(self):
        marks = self.__spacing * 0.3

        # Draws grid
        for y in range(self.__spacing, _CANVAS_LENGTH, self.__spacing):
            self.__draw.line([y, 0, y, _CANVAS_LENGTH], _GREY)  # vertical line (y-axis)
        for x in range(self.__spacing, _CANVAS_LENGTH, self.__spacing):
            self.__draw.line([0, x, _CANVAS_LENGTH, x], _GREY)  # horizontal line (x-axis)

        # Draws markers
        # y-axis
        counter = 1.0
        for y in range(self.__spacing, _CANVAS_LENGTH, self.__spacing):
            self.__draw.line([(_HALF_CANVAS_LENGTH - marks), y, (_HALF_CANVAS_LENGTH + marks), y], _BLACK)

            # Draws marker's values on y-axis
            if self.__show_marker_value:
                self.__draw.text((_HALF_CANVAS_LENGTH + y, _HALF_CANVAS_LENGTH + marks * 2),
                                 str(float(self.__ysteps) * counter), fill=(0, 0, 0))
                self.__draw.text((_HALF_CANVAS_LENGTH - y, _HALF_CANVAS_LENGTH + marks * 2),
                                 str(float(self.__ysteps) * counter * -1.), fill=(0, 0, 0))
                counter += 1.0

        # x-axis
        counter = 1.0
        for x in range(self.__spacing, _CANVAS_LENGTH, self.__spacing):
            self.__draw.line([x, (_HALF_CANVAS_LENGTH + marks), x, (_HALF_CANVAS_LENGTH - marks)], _BLACK)

            # Draws marker's values on x-axis
            if self.__show_marker_value:
                self.__draw.text((_HALF_CANVAS_LENGTH + marks * 2, _HALF_CANVAS_LENGTH + x),
                                 str(float(self.__xsteps) * counter * -1.0), fill=(0, 0, 0))
                self.__draw.text((_HALF_CANVAS_LENGTH + marks * 2, _HALF_CANVAS_LENGTH - x),
                                 str(float(self.__xsteps) * counter), fill=(0, 0, 0))
                counter += 1.0

        # Draws x-y lines
        self.__draw.line([0, _HALF_CANVAS_LENGTH, _CANVAS_LENGTH, _HALF_CANVAS_LENGTH], _BLACK)
        self.__draw.line([_HALF_CANVAS_LENGTH, 0, _HALF_CANVAS_LENGTH, _CANVAS_LENGTH], _BLACK)

    # PRIVATE: Draws board's functions #################################################################################
    def __draw_functions(self):
        # self.controller.board.functions should be drawn
        for function in self.__controller.board.function_list:
            if function.visible:
                thread = threading.Thread(target=self.__draw_function_thread, args=(function,))
                thread.start()

    # PRIVATE: Starts a thread for the given function ##################################################################
    def __draw_function_thread(self, function):
        coords_array = Parse.calc(function.function,
                                  prec=self.__precision,
                                  xrange=self.__xrange,
                                  yrange=self.__yrange,
                                  xsteps=self.__xsteps,
                                  ysteps=self.__ysteps)
        self.__draw_function(coords_array, function.color)

    # PRIVATE: Draws board's function with normalized coordinates ######################################################
    # Param coord_array: Array with coordinates
    # Param color: Color of function
    def __draw_function(self, coord_array, color):
        for x in range(0, len(coord_array) - 1, 1):
            # checks if the given x or y value is infinity
            if coord_array[x][1] == 'inf' or coord_array[x+1][1] == 'inf' or self.__check_difference(coord_array[x],
                                                                                                     coord_array[x+1]):
                continue
            self.__canvas.create_line(
                self.__normalize(coord_array[x][0]),
                self.__normalize(-coord_array[x][1]),
                self.__normalize(coord_array[x + 1][0]),
                self.__normalize(-coord_array[x + 1][1]),
                fill=color)
            self.__draw.line(
                [self.__normalize(coord_array[x][0]),
                 self.__normalize(-coord_array[x][1]),
                 self.__normalize(coord_array[x + 1][0]),
                 self.__normalize(-coord_array[x + 1][1])],
                self.__get_rbg_color(color))

    # PROTECTED: Projects mathematical values to graphical coordinates #################################################
    def __normalize(self, value):
        val = value * self.__spacing + _CENTER
        return val

    ####################################################################################################################
    # Button method section ############################################################################################

    ####################################################################################################################
    # Board section ####################################################################################################

    # PRIVATE: Adds canvas transformation buttons ######################################################################
    def __add_transformation_buttons(self):
        self.__grid_frame = Frame(self.__main_window, bg=self.__get_hex_color(_GREEN))
        self.__grid_frame.grid(column=0, row=0, sticky=N + E + W, pady=(10, 10))  # main grid

        # Zoom
        self.__button1 = Button(self.__grid_frame,
                                text='Zoom out',
                                state='disabled',
                                command=lambda: self.__method_picker('_zoom_canvas', -1))
        self.__button1.grid(column=0, row=1, sticky=N + S + E + W)  # sticky fills the width gap / sub grid
        self.__button2 = Button(self.__grid_frame,
                                text='Zoom in',
                                state='disabled',
                                command=lambda: self.__method_picker('_zoom_canvas', 1))
        self.__button2.grid(column=1, row=1, sticky=N + S + E + W)

        # Precision
        self.__button3 = Button(self.__grid_frame,
                                text='Precision down',
                                state='disabled',
                                command=lambda: self.__method_picker('_change_precision', 1.1))
        self.__button3.grid(column=0, row=2, sticky=N + S + E + W)
        self.__button4 = Button(self.__grid_frame,
                                text='Precision up',
                                state='disabled',
                                command=lambda: self.__method_picker('_change_precision', 0.9))
        self.__button4.grid(column=1, row=2, sticky=N + S + E + W)

        self.__grid_subframe = Frame(self.__grid_frame, bg=self.__get_hex_color(_GREEN))
        self.__grid_subframe.grid(column=0, row=3, columnspan=2, sticky=N + E + W, pady=(10, 10))

        # xrange
        __label1 = Label(self.__grid_subframe, text='range of x')
        __label1.grid(column=0, row=0, sticky=N + S + E + W)

        self.__entry1 = Entry(self.__grid_subframe, width=10)
        self.__entry1.grid(column=1, row=0, sticky=N + S + E + W)

        self.__button5 = Button(self.__grid_subframe,
                                text='set',
                                state='disabled',
                                command=lambda: self.__method_picker('_change_xrange', self.__entry1.get()))
        self.__button5.grid(column=2, row=0, sticky=N + S + E + W)

        # yrange
        __label2 = Label(self.__grid_subframe, text='range of y')
        __label2.grid(column=0, row=1, sticky=N + S + E + W)

        self.__entry2 = Entry(self.__grid_subframe, width=10)
        self.__entry2.grid(column=1, row=1, sticky=N + S + E + W)

        self.__button6 = Button(self.__grid_subframe,
                                text='set',
                                state='disabled',
                                command=lambda: self.__method_picker('_change_yrange', self.__entry2.get()))
        self.__button6.grid(column=2, row=1, sticky=N + S + E + W)

        # xsteps
        __label3 = Label(self.__grid_subframe, text='steps on x axis')
        __label3.grid(column=0, row=2, sticky=N + S + E + W)

        self.__entry3 = Entry(self.__grid_subframe, width=10)
        self.__entry3.grid(column=1, row=2, sticky=N + S + E + W)

        self.__button7 = Button(self.__grid_subframe,
                                text='set',
                                state='disabled',
                                command=lambda: self.__method_picker('_change_xsteps', self.__entry3.get()))
        self.__button7.grid(column=2, row=2, sticky=N + S + E + W)

        # ysteps
        __label4 = Label(self.__grid_subframe, text='steps on y axis')
        __label4.grid(column=0, row=3, sticky=N + S + E + W)

        self.__entry4 = Entry(self.__grid_subframe, width=10)
        self.__entry4.grid(column=1, row=3, sticky=N + S + E + W)

        self.__button8 = Button(self.__grid_subframe,
                                text='set',
                                state='disabled',
                                command=lambda: self.__method_picker('_change_ysteps', self.__entry4.get()))
        self.__button8.grid(column=2, row=3, sticky=N + S + E + W)

    # Changes range value on x-axis ####################################################################################
    # Param value: New range value
    def _change_xrange(self, value):
        try:
            self.__xrange = float(value) if value else _DEFAULT_X_RANGE
            self.__update_canvas()
        except ValueError:
            self._show_error('Value must be a number.')

    # Changes range value on y-axis ####################################################################################
    # Param value: New range value
    def _change_yrange(self, value):
        try:
            self.__yrange = float(value) if value else _DEFAULT_Y_RANGE
            self.__update_canvas()
        except ValueError:
            self._show_error('Value must be a number.')

    # Changes steps value on x-axis ####################################################################################
    # Param value: New step value
    def _change_xsteps(self, value):
        try:
            self.__xsteps = float(value) if value else _DEFAULT_X_STEPS
            self.__update_canvas()
        except ValueError:
            self._show_error('Value must be a number.')

    # Changes steps value on y-axis ####################################################################################
    # Param value: New step value
    def _change_ysteps(self, value):
        try:
            self.__ysteps = float(value) if value else _DEFAULT_Y_STEPS
            self.__update_canvas()
        except ValueError:
            self._show_error('Value must be a number.')

    # Adds third column on the main window containing all known functions ##############################################
    def __add_function_buttons(self):
        self.__function_frame = Frame(self.__main_window)
        self.__function_frame.grid(column=2, row=0, sticky=N + E + W, pady=(10, 10))

        row_counter = 1

        for function in self.__controller.get_board_functions():
            func_frame = Frame(self.__function_frame, width=150)
            if function.visible:
                func_frame.config(bg=self.__get_hex_color(_GREEN))
            else:
                func_frame.config(bg=self.__get_hex_color(_RED))
            func_frame.grid(row=row_counter, pady=(0, 5))

            # function label
            func_label = Label(func_frame, text=function.function)
            func_label.grid(row=0, columnspan=5)

            # delete button
            func_delete = Button(func_frame, text='delete',
                                 command=lambda i=row_counter: self.__method_picker('_delete_function', i))
            func_delete.grid(column=4, row=1, sticky=N + S + E + W)  # bottom right

            # visibility button
            func_vis = Button(func_frame, text='show/hide',
                              command=lambda i=row_counter, j=func_frame:
                              self.__method_picker('_change_func_visibility', i, func_frame))
            func_vis.grid(column=0, row=1, sticky=E)

            # color button
            func_color = Button(func_frame, text='color',
                                command=lambda i=row_counter: self.__method_picker('_change_func_color', i))
            func_color.grid(column=1, row=1)

            row_counter += 1

    # Destroys active function button frame. Is called when loading another board ######################################
    def __delete_function_frame(self):
        self.__function_frame.destroy()

    # Adds a new function to board #####################################################################################
    def _add_function(self):
        if self.__controller.examine_board_selected():
            function = simpledialog.askstring("New Function", "Please enter a function", parent=self.__main_window)
            if function is not None:
                self.__controller.new_function(function)
                self.__update_canvas()  # resets canvas / pil file
                messagebox.showinfo('New Function',
                                    'Function \'{name}\' successfully created.'.format(name=function.strip()))

    # Changes the color of a function ##################################################################################
    # Param idx: Array index of function
    def _change_func_color(self, idx):
        rgb_color, hex_color = colorchooser.askcolor(parent=self.__main_window, initialcolor=(255, 0, 0))
        if hex_color:
            func_id = self.__controller.board.get_id_by_idx(int(idx))
            self.__controller.change_function_color(int(func_id), hex_color)
            self.__update_canvas()

    # Changes the visibility of a function #############################################################################
    # Param idx: Array index of function
    def _change_func_visibility(self, idx, func_frame):
        func_id = self.__controller.board.get_id_by_idx(int(idx))
        self.__controller.change_function_visibility(int(func_id))

        # checks visibility and changes background red/green
        if self.__controller.get_function_visibility(idx):
            func_frame.config(bg=self.__get_hex_color(_GREEN))
        else:
            func_frame.config(bg=self.__get_hex_color(_RED))
        self.__update_canvas()

    # Deletes a function ###############################################################################################
    # Param idx: Array index of function
    def _delete_function(self, idx):
        if messagebox.askyesno('Delete function', 'Really delete this function?'):
            func_id = self.__controller.board.get_id_by_idx(int(idx))
            self.__controller.delete_function(int(func_id))
            self.__update_canvas()

    # Creates a new board ##############################################################################################
    def _new_board(self):
        board_name = simpledialog.askstring("New Board", "Please enter a name", parent=self.__main_window)
        if board_name is not None:
            if self.__function_frame is not None:  # is true when a board is already loaded with functions
                self.__delete_function_frame()
            self.__controller.new_board(board_name.strip())
            self.__update_canvas()  # resets canvas / pil file
            messagebox.showinfo('New board', 'Board \'{name}\' successfully created.'.format(name=board_name.strip()))
            self.__activate_buttons()
            self.__main_window.title("Function Plotter, Active Board: " + self.__controller.board.name)
            self._add_function()

    # Opens board delete dialog window #################################################################################
    def _delete_board_window(self):
        window = self._new_window(self.__main_window, 'Delete Function')
        window_frame = Frame(window)
        window_frame.pack()

        scrollbar = Scrollbar(window_frame, orient='vertical')
        scrollbar.pack(side=RIGHT, fill=Y)

        board_list_box = Listbox(window_frame, selectmode=SINGLE, yscrollcommand=scrollbar.set, width=60, height=10)
        board_list_box.pack(side=LEFT)
        for name in self.__controller.get_all_boards():  # id, name
            board_list_box.insert(END, name)

        scrollbar.config(command=board_list_box.yview)
        window_frame2 = Frame(window)
        window_frame2.pack()
        board_delete = Button(window_frame2, text='delete selected',
                              command=lambda: self.__delete_board(window,
                                                                  board_list_box.get(board_list_box.curselection())))
        board_delete.pack()

    # Deletes given board from the database through the controller #####################################################
    def __delete_board(self, window, var):
        self.__controller.delete_board(var[0])
        messagebox.showinfo('Board deleted', message='Board \'' + var[1] + '\' successfully deleted.')
        window.destroy()
        self.__update_canvas()

    # Saves active board to database ###################################################################################
    def _save_board(self):
        if self.__controller.examine_board_selected():
            self.__controller.save_board_to_database()
            messagebox.showinfo("Save", "Save successful.")
        else:
            messagebox.showerror('No board selected.')

    # Creates an image file of the active plot window ##################################################################
    # Image formats: JPG, PNG, BMP, GIF
    def _export_board(self):
        container_window = self._new_window(self.__main_window, 'Export canvas into a picture')
        frame = Frame(container_window)

        window_width = 680
        window_height = 55
        # Calculates window x coordinate
        spawn_x, spawn_y = self.__calc_layer_coords(window_width)
        # Adds coordinates to window position
        container_window.geometry("%dx%d+%d+%d" % (window_width, window_height, spawn_x, spawn_y))

        type_variable = StringVar(frame)
        type_variable.set(_TYPES[0])  # default value

        name_variable = StringVar(frame)
        name_variable.set(os.getcwd() + "\\my_filename")

        Entry(frame, textvariable=name_variable, width=100).grid(row=1, column=1, sticky='n')
        OptionMenu(frame, type_variable, *_TYPES).grid(row=1, column=2, sticky='n')
        Button(frame, text='Save', width=50,
               command=lambda: self.__save_export(name_variable.get(), type_variable.get())).grid(row=2, sticky='s',
                                                                                                  columnspan=2)
        frame.pack()

    # Saves the given file to the harddrive and opens it ###############################################################
    # Param filen: Path of the file
    # Param filet: Data type of file
    def __save_export(self, filen, filet):
        # creates the PIL image/draw (in memory) drawings
        filename = filen + filet
        self.__image_tmp.save(filename)  # creates the picture out of the postscript file
        # to test, view the saved file, works with Windows only
        # behaves like double-clicking on the saved file
        os.startfile(filename)  # opens picture for viewing pleasure

    # Gives the user a choice between saved boards to load one of them #################################################
    def _load_board_menu(self):
        if self.__controller.board is not None:
            msg = 'Warning: a board is already opened. Unsaved changes won\'t be committed to the database.'
            messagebox.showwarning('A board is open', msg)

        window = self._new_window(self.__main_window, 'Load Board')
        Label(window, text='Load one of the following boards:', justify=tk.LEFT, padx=20).pack()
        window_frame = Frame(window)
        window_frame.pack()

        scrollbar = Scrollbar(window_frame, orient='vertical')
        scrollbar.pack(side=RIGHT, fill=Y)

        board_list_box = Listbox(window_frame, selectmode=SINGLE, yscrollcommand=scrollbar.set, width=60, height=10)
        board_list_box.pack(side=LEFT)

        for name in self.__controller.get_all_boards():
            board_list_box.insert(END, name)

        scrollbar.config(command=board_list_box.yview)
        window_frame2 = Frame(window)
        window_frame2.pack()
        load_board_button = Button(window_frame2, text='load selected',
                                   command=lambda: self.__load_board(window,
                                                                     board_list_box.get(board_list_box.curselection())))
        load_board_button.pack()

    # Loads the selected board #########################################################################################
    def __load_board(self, window, v):
        self.__controller.discard_changes()
        self.__controller.load_board(v[0])
        window.destroy()
        if self.__function_frame is not None:
            self.__delete_function_frame()
        messagebox.showinfo('Load', 'Board \"' + v[1] + '\" successfully loaded.')
        self.__update_canvas()
        self.__activate_buttons()
        self.__main_window.title("Function Plotter, Active Board: " + self.__controller.board.name)

    # PRIVATE: Activates disabled buttons ##############################################################################
    def __activate_buttons(self):
        self.__button1.config(state="normal")
        self.__button2.config(state="normal")
        self.__button3.config(state="normal")
        self.__button4.config(state="normal")
        self.__button5.config(state="normal")
        self.__button6.config(state="normal")
        self.__button7.config(state="normal")
        self.__button8.config(state="normal")

    # Opens a window that shows information about this project #########################################################
    def _open_about_window(self):
        about_level = self._new_window(self.__main_window, 'About')
        about_level.resizable(FALSE, FALSE)
        about_width = 250
        about_height = 150
        # Calculates window x coordinates
        about_spawn_x, about_spawn_y = self.__calc_layer_coords(about_width)
        # Adds coordinates to window position
        about_level.geometry("%dx%d+%d+%d" % (about_width, about_height, about_spawn_x, about_spawn_y))

        # Frame containing the text
        about_main_frame = Frame(about_level, pady=15)

        # Adds labels to the frame
        Label(about_main_frame, text=resources.description)
        Label(about_main_frame, text='Version: ' + str(resources.version))
        Label(about_main_frame, text="Created by")
        Label(about_main_frame, text=resources.authors)

        # Adds a close button to the top level window
        Button(about_level, text="Close", command=about_level.destroy)

        # Packs all children of about_main_frame
        self.__packer(about_level.winfo_children())

    ####################################################################################################################
    # Utilities that are used more often than once #####################################################################

    # Creates a new top level window, awaits a parent window ###########################################################
    # Param dependency: Dependency of main window
    # Param name: New window name
    # Returns a new child window
    @staticmethod
    def _new_window(dependency, name):
        window = Toplevel(dependency)
        window.title = name
        return window

    # Displays error message as popup dialog ###########################################################################
    # Param error: Error message as string
    @staticmethod
    def _show_error(error):
        messagebox.showerror('Error', error)

    # Takes a color hex value and returns it's values as RGB tuple #####################################################
    # Returns RGB color value as tuple
    @staticmethod
    def __get_rbg_color(hex_color):
        hex_s = hex_color.lstrip('#')
        return tuple(int(hex_s[i:i + 2], 16) for i in (0, 2, 4))  # generation

    # Takes a color RGB value and returns it's values as a hex value ###################################################
    # Returns hexadecimal color value
    @staticmethod
    def __get_hex_color(rgb):
        return '#%02x%02x%02x' % rgb

    # Calculates x and y position of the window ########################################################################
    # Param window_width: Width of window
    # Returns array (x, y)
    def __calc_layer_coords(self, window_width):
        window_x = self.__main_window.winfo_rootx() + self.__main_window.winfo_width() / 2 - window_width / 2
        window_y = self.__main_window.winfo_rooty() + 50
        return window_x, window_y

    # Recursively takes a list of child nodes and packs them to the window / frame #####################################
    # Param child_list: Array with child nodes of window
    def __packer(self, child_list):
        for child in child_list:
            child.pack()
            if len(child.winfo_children()) > 0:
                self.__packer(child.winfo_children())

    # Function chooser catches every exception and calls given method ##################################################
    # Param method_name: Name of the method to call
    # Param *argv: Array list of committed method parameters
    def __method_picker(self, method_name, *argv):
        method = method_name
        try:
            method = getattr(self, method)
            if argv is not None:
                method(*argv)
            else:
                method()
        except AttributeError:
            self._show_error("Class `{}` does not implement `{}`".format(self.__class__.__name__, method))
        except Exception as e:
            self._show_error(e)

    # PRIVATE: Check if difference between two coordinates too big #####################################################
    # Param coord1: x coordinate value
    # Param coord2: next x coordinate value
    # Returns boolean
    def __check_difference(self, coord1, coord2):
        n_coord1 = []
        n_coord2 = []

        for val in coord1:
            if val < 0:
                val *= -1.0
                n_coord1.append(val)

        for val in coord2:
            if val < 0:
                val *= -1.0
                n_coord2.append(val)

        difference = []
        diff_x = coord1[0] - coord2[0]

        if diff_x < 0:
            diff_x *= -1.0
        diff_y = coord1[1] - coord2[1]
        if diff_y < 0:
            diff_y *= -1.0

        difference.append(diff_x)
        difference.append(diff_y)

        if difference[0] > 3/self.__precision or difference[1] > 3/self.__precision:
            return True
        else:
            return False
