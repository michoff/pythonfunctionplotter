

# Calculates working zoom factor numbers ###############################################################################
# If other values are used for zoom, the coordinate system will not be printed the right way
def array_func(canvas_size):
    array = []
    for i in range(1, canvas_size, 1):
        if canvas_size % i == 0:
            array.append(i)
    for i in array:
        if (canvas_size / i) % 2 == 1:
            array.remove(i)
    return array
